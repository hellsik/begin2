﻿using ContactMenager.Models;
using ContactMenager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ContactMenager.Controllers
{
    public class ContactController : ApiController
    {
        private ContactRepository contactRepository;

        public ContactController()
        {
            this.contactRepository = new ContactRepository();
        }
        // GET: api/Contact
        public IEnumerable<Contact> Get()
        {
            return contactRepository.GetAllContacts();
        }

        // GET: api/Contact/5
        public Contact Get(int id)
        {
            return contactRepository.GetContact(id);
        }

        // POST: api/Contact
        public HttpResponseMessage Post([FromBody]Contact contact)
        {
            contactRepository.SaveContact(contact);
            var response = Request.CreateResponse<Contact>(System.Net.HttpStatusCode.Created, contact);

            return response;
        }

        // PUT: api/Contact/5
        public void Put(int id, [FromBody]Contact contact)
        {
        }

        // DELETE: api/Contact/5
        public void Delete(int id)
        {
            contactRepository.DeleteContact(id);
        }
    }
}
