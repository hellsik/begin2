﻿using ContactMenager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactMenager.Services
{
    public class ContactRepository
    {
        private List<Contact> contacts { get; set; }

        public ContactRepository()
        {
            contacts = new List<Contact> {
                new Contact { Id = 1, Name = "Hubert Oleksiuk" },
                new Contact { Id = 2, Name = "Karol Ornoch" }
            };
        }

        public List<Contact> GetAllContacts()
        {
            return contacts;
        }

        public bool SaveContact(Contact contact)
        {
            if (contact != null)
            {
                contacts.Add(contact);
                return true;
            }
            return false;
        }
        public Contact GetContact(int id)
        {
            return contacts.Single(c => c.Id == id);
        }

        public void DeleteContact(int id)
        {
            contacts.RemoveAt(id);
        }

    }
}